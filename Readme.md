Magento WordPress Integration is the easiest way to get blocks, sessions and products from your Magento store.

Description
===========

Magento WordPress Integration allows you to add any Magento blocks to your WordPress theme, including ones you have created yourself, and static blocks created through the Magento admin area. 

The Magento WordPress Integration Plugin allows you to to the following:

*   Bring out any of the default Magento Blocks in your WordPress theme.
*   Bring out any Magento blocks that you have created yourself.
*   Bring out any static blocks that you have made in your Magento admin area.
*   Show Magento products on a WordPress post or page by using the shortcode add-on.
*   Show products from any category using the widget add-on.

Add Ons
=======

There are also a number of add-ons available for MWI, these are:

*	**[Responsive Product Slider](http://www.mwi-plugin.com/add-ons/responsive-product-slider/)** This add-on allows you to display any number of products from a Magento category in a responsive slider, using a shortcode.
*	**[Shortcodes & Widgets](http://www.mwi-plugin.com/add-ons/shortcodes-widgets/)** This add-on allows you to display products on your WordPress posts and pages using a very simple shortcode. It also comes with a widget for displaying products from a Magento category.
*	**[Category Specific Widget](http://www.mwi-plugin.com/add-ons/category-specific-widget/)** The Category Specific add-on allows you to associate a Magento category with a WordPress category, so relevant products can be displayed within WordPress category or post pages.

Useful Links
============

* 	[Support](http://wordpress.org/support/plugin/magento-wordpress-integration/)
*	[FAQ](http://mwi-plugin.com/documentation/faq/)
*	[MWI Demo](http://demo.mwi-plugin.com/)
*	[Single Sign-on for Magento and Wordpress](http://codecanyon.net/item/single-signon-for-magento-and-wordpress/861453?ref=jamesckemp)

Notes
=====

*	This plugin does not allow you to completely avoid using Magento on your website, it simply helps you to get your WordPress and Magento themes seamlessly integrated.
*	**If upgrading, please back up your database first!**
*	Follow me on Twitter to keep up with the latest updates [@jamesckemp](https://twitter.com/#!/jamesckemp/)